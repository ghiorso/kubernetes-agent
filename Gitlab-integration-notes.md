# Gitlab Cluster Integration Notes

## 4/9/2021  
Prometheus was previously install as a managed app on the ENKIserver cluster. This will be changed to an integrated service, which will allow linkage to a more recent installation of Prometheus on teh cluster that is self managed and includes Grafana (i.e. the "kube-prometheus-stack installation via Helm).

- On the Gitlab repository portal, operations->kubernetes
  - Click on ENKIserver cluster and choose the *Applications* tab
  - Uninstall Gitlab Runner
  - Uninstall Prometheus
  - Now, "Enable Prometheus Integration" appears as an option on the Integrations tab. Follow the instructions found in the link "documented process."
  
```
# Create the require Kubernetes namespace
kubectl create ns gitlab-managed-apps

# Download Helm chart values that is compatible with the requirements above.
# You should substitute the tag that corresponds to the GitLab version in the url
# - https://gitlab.com/gitlab-org/gitlab/-/raw/<tag>/vendor/prometheus/values.yaml
#
wget https://gitlab.com/gitlab-org/gitlab/-/raw/v13.9.0-ee/vendor/prometheus/values.yaml

# Add the Prometheus community helm repo
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

# Install Prometheus
helm install prometheus prometheus-community/prometheus -n gitlab-managed-apps --values values.yaml
```

These commands returned:
```
NAME: prometheus
LAST DEPLOYED: Fri Apr  9 16:52:20 2021
NAMESPACE: gitlab-managed-apps
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
The Prometheus server can be accessed via port 80 on the following DNS name from within your cluster:
prometheus-prometheus-server.gitlab-managed-apps.svc.cluster.local


Get the Prometheus server URL by running these commands in the same shell:
  export POD_NAME=$(kubectl get pods --namespace gitlab-managed-apps -l "app=prometheus,component=server" -o jsonpath="{.items[0].metadata.name}")
  kubectl --namespace gitlab-managed-apps port-forward $POD_NAME 9090


#################################################################################
######   WARNING: Pod Security Policy has been moved to a global property.  #####
######            use .Values.podSecurityPolicy.enabled with pod-based      #####
######            annotations                                               #####
######            (e.g. .Values.nodeExporter.podSecurityPolicy.annotations) #####
#################################################################################



For more information on running Prometheus, visit:
https://prometheus.io/
```
And, clicking the integration checkbox on the GitLab repository interface gives successful integration.

Next, go to Google Cloud dashboard, Kubernetes
- Services & Ingress, click on prometheus-prometheus-server
- Click on prometheus-prometheus-server under deployments
    - From the actions menu, choose Expose
    - Set target port at 9090, exposed port at 60000
    - Set IP to load balancing
    - Expost the port
- Goto Grafana Lab (set up free account previously)
    - Add prometheus data source correcponding to exposed port above)
    - Add Dashboards
- Return to GitLab.com repository interface
    - In settings->operations, open "metric dashboards" and add https://enkiserver.grafana.com as a source

## 4/12/21

[Kubernetes Agent Installation](https://docs.gitlab.com/ee/user/clusters/agent/#define-a-configuration-repository)

- Create a repository called *kubernetes-agent* at GitLab.com
- Add .gitlab/agents/enki_agent/config.yaml file
```
gitops:
  manifest_projects:
  - id: "ghiorso/kubernetes-agent"
```
- Create an agent record in [GraphiQL](https://gitlab.com/-/graphql-explorer). Two stages. Stage 1:
```
mutation createAgent {
    # agent-name should be the same as specified above in the config.yaml
    createClusterAgent(input: { projectPath: "ghiorso/kubernetes-agent", name: "enki-agent" }) {
      clusterAgent {
        id
        name
      }
      errors
    }
  }
```
Output:
```
{
  "data": {
    "createClusterAgent": {
      "clusterAgent": {
        "id": "gid://gitlab/Clusters::Agent/96",
        "name": "enki-agent"
      },
      "errors": []
    }
  }
}
```
- Stage 2 (generic record):
```
mutation createToken {
    clusterAgentTokenCreate(
      input: {
        clusterAgentId: "<cluster-agent-id-taken-from-the-previous-mutation>"
        description: "<optional-description-of-token>"
        name: "<required-name-given-to-token>"
      }
    ) {
      secret # This is the value you need to use on the next step
      token {
        createdAt
        id
      }
      errors
    }
  }
```
- Stage 2 (specific record):
```
mutation createToken {
    clusterAgentTokenCreate(
      input: {
        clusterAgentId: "gid://gitlab/Clusters::Agent/96"
        description: "ENKI GKE Cluster"
        name: "enki-agent"
      }
    ) {
      secret # This is the value you need to use on the next step
      token {
        createdAt
        id
      }
      errors
    }
}
```
Output:
```
{
  "data": {
    "clusterAgentTokenCreate": {
      "secret": "itQE4zJYu5WNY-T2whC-WzVRWCGAwhArmzyey-SUchQMusxzew",
      "token": {
        "createdAt": "2021-04-13T03:44:30Z",
        "id": "gid://gitlab/Clusters::AgentToken/95"
      },
      "errors": []
    }
  }
}
```
- In cloud shell on the Google Cloud dashboard:
    - Image container registry:  https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/container_registry
    - most recent cli container is v13.10.1
    - run the command (generic):
    ```
    docker run \
    --pull=always \
    --rm registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:stable \
    generate \
    --agent-token=your-agent-token \
    --kas-address=wss://kas.gitlab.com \
    --agent-version stable | kubectl apply -f -
    ```
    - (specific)
    ```
    docker run \
    --pull=always \
    --rm registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:v13.10.1 \
    generate \
    --agent-token=itQE4zJYu5WNY-T2whC-WzVRWCGAwhArmzyey-SUchQMusxzew \
    --kas-address=wss://kas.gitlab.com \
    --agent-version v13.10.1 | kubectl apply -f -
    ```
Output:
```
docker run --pull=always --rm registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:v13.10.1 generate --agent-token=itQE4zJYu5WNY-T2whC-WzVRWCGAwhArmzyey-SUchQMusxzew --kas-address=wss://kas.gitlab.com --agent-version v13.10.1 | kubectl apply -f -
v13.10.1: Pulling from gitlab-org/cluster-integration/gitlab-agent/cli
d94d38b8f0e6: Pulling fs layer
c3c40d5e8c96: Pulling fs layer
0f41bae7d828: Pulling fs layer
828e2783a94d: Pulling fs layer
3350ab7677e9: Pulling fs layer
db8e99d0a931: Pulling fs layer
94d03d3ba937: Pulling fs layer
828e2783a94d: Waiting
3350ab7677e9: Waiting
db8e99d0a931: Waiting
94d03d3ba937: Waiting
d94d38b8f0e6: Download complete
0f41bae7d828: Verifying Checksum
0f41bae7d828: Download complete
c3c40d5e8c96: Verifying Checksum
c3c40d5e8c96: Download complete
d94d38b8f0e6: Pull complete
db8e99d0a931: Verifying Checksum
db8e99d0a931: Download complete
c3c40d5e8c96: Pull complete
828e2783a94d: Verifying Checksum
828e2783a94d: Download complete
0f41bae7d828: Pull complete
94d03d3ba937: Verifying Checksum
94d03d3ba937: Download complete
3350ab7677e9: Verifying Checksum
3350ab7677e9: Download complete
828e2783a94d: Pull complete
3350ab7677e9: Pull complete
db8e99d0a931: Pull complete
94d03d3ba937: Pull complete
Digest: sha256:c2f50d1f01a3bed5b0fe3a487dbbf7e3f5e6e944dc1713c99e2318392737ca4f
Status: Downloaded newer image for registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:v13.10.1
clusterrole.rbac.authorization.k8s.io/cilium-alert-read created
clusterrole.rbac.authorization.k8s.io/gitlab-agent-gitops-read-all created
clusterrole.rbac.authorization.k8s.io/gitlab-agent-gitops-write-all created
clusterrolebinding.rbac.authorization.k8s.io/cilium-alert-read created
clusterrolebinding.rbac.authorization.k8s.io/gitlab-agent-gitops-read-all created
clusterrolebinding.rbac.authorization.k8s.io/gitlab-agent-gitops-write-all created
Error from server (NotFound): error when creating "STDIN": namespaces "gitlab-agent" not found
Error from server (NotFound): error when creating "STDIN": namespaces "gitlab-agent" not found
Error from server (NotFound): error when creating "STDIN": namespaces "gitlab-agent" not found

ghiorso@cloudshell:~ (pelagic-script-244221)$ kubectl create namespace gitlab-agent
namespace/gitlab-agent created

ghiorso@cloudshell:~ (pelagic-script-244221)$ docker run --pull=always --rm registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:v13.10.1 generate --agent-token=itQE4zJYu5WNY-T2whC-WzVRWCGAwhArmzyey-SUchQMusxzew --kas-address=wss://kas.gitlab.com --agent-version v13.10.1 | kubectl apply -f -

v13.10.1: Pulling from gitlab-org/cluster-integration/gitlab-agent/cli
Digest: sha256:c2f50d1f01a3bed5b0fe3a487dbbf7e3f5e6e944dc1713c99e2318392737ca4f
Status: Image is up to date for registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:v13.10.1
serviceaccount/gitlab-agent created
clusterrole.rbac.authorization.k8s.io/cilium-alert-read unchanged
clusterrole.rbac.authorization.k8s.io/gitlab-agent-gitops-read-all unchanged
clusterrole.rbac.authorization.k8s.io/gitlab-agent-gitops-write-all unchanged
clusterrolebinding.rbac.authorization.k8s.io/cilium-alert-read unchanged
clusterrolebinding.rbac.authorization.k8s.io/gitlab-agent-gitops-read-all unchanged
clusterrolebinding.rbac.authorization.k8s.io/gitlab-agent-gitops-write-all unchanged
secret/gitlab-agent-token-khm2ff582k created
deployment.apps/gitlab-agent created
```
Note that in the above instructions from GitLab docs, an error occurs because the *gitlab-agent* namespace does not exist. It should be created first.

## Not Yet Implemented

This installation is intended for self-managed repositories.
    
Install manifest file for GitLab runner
- Make a [GitLab Chart YAML file](https://gitlab.com/gitlab-org/charts/gitlab-runner/blob/master/values.yaml), like *runner-chart-values.yaml*:
```
# The GitLab Server URL (with protocol) that want to register the runner against
# ref: https://docs.gitlab.com/runner/commands/README.html#gitlab-runner-register
#
gitlabUrl: https://gitlab.my.domain.example.com/

# The Registration Token for adding new runners to the GitLab Server. This must
# be retrieved from your GitLab instance.
# ref: https://docs.gitlab.com/ce/ci/runners/README.html
#
runnerRegistrationToken: "yrnZW46BrtBFqM7xDzE7dddd"

# For RBAC support:
rbac:
    create: true

# Run all containers with the privileged flag enabled
# This will allow the docker:dind image to run if you need to run Docker
# commands. Please read the docs before turning this on:
# ref: https://docs.gitlab.com/runner/executors/kubernetes.html#using-dockerdind
runners:
    privileged: true
```
- Create a single manifest file to install the GitLab Runner chart with your cluster agent, replacing GITLAB GITLAB-RUNNER with your namespace:
```
helm template --namespace GITLAB GITLAB-RUNNER -f runner-chart-values.yaml gitlab/gitlab-runner > runner-manifest.yaml
```
- Push your runner-manifest.yaml to your manifest repository